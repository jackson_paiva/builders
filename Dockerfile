FROM gradle:6.8.1-jdk11 as builder-gradle
ENV APP_HOME=/root/dev/app/
WORKDIR $APP_HOME
COPY . .
RUN gradle build -x test

FROM springci/graalvm-ce:master-java11
ENV APP_NAME builder.jar
ENV JAR=/root/dev/app/build/libs
COPY --from=builder-gradle $JAR .
EXPOSE 8080
ENTRYPOINT exec java -jar $APP_NAME