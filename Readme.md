## Test for Platform Builder
Test for Platform Builder

### Run app
- docker-compose up --build

### Stop app
- docker-compose down

### Collection Postman
[platform_builder.postman_collection.json](Collection Postman)