package com.builder.test.mapper;

import com.builder.test.domain.Client;
import com.builder.test.dto.ClientDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    ClientDTO clientToClientDTO(Client client);

    Client clientDTOToClient(ClientDTO clientDTO);

    List<ClientDTO> clientToClientDTO(List<Client> client);
}
