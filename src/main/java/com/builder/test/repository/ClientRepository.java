package com.builder.test.repository;

import com.builder.test.domain.Client;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ClientRepository extends CrudRepository<Client, String>, QuerydslPredicateExecutor<Client> {

    Client findById(UUID uuid);

}
