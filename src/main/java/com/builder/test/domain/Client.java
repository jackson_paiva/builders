package com.builder.test.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.UUID;


@QueryEntity
@Data
@Entity
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false, nullable = false)
    private UUID id;
    private String name;
    private String cpf;

    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate birthDate;

    @Transient
    private String age;

    @Transient
    public Integer getAge(){
        Period diff = Period.between(this.birthDate, LocalDate.now());
        return diff.getYears();
    }
}
