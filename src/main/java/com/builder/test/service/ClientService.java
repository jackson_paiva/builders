package com.builder.test.service;

import com.builder.test.domain.Client;
import com.builder.test.dto.ClientDTO;
import com.builder.test.mapper.ClientMapper;
import com.builder.test.repository.ClientRepository;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Service
public class ClientService {

    private final ClientRepository userRepository;
    private ClientMapper clientMapper;

    public Client getById(UUID id) {
        return userRepository.findById(id);
    }

    public List<ClientDTO> findAllPaginator(Pageable pageable, BooleanBuilder booleanBuilder) {
        Page<Client> clientPages = userRepository.findAll(booleanBuilder, pageable);
        return clientMapper.clientToClientDTO(clientPages.toList());
    }

    public ClientDTO clientSave(ClientDTO clientDTO) {
        Client client = clientMapper.clientDTOToClient(clientDTO);
        client = userRepository.save(client);
        return clientMapper.clientToClientDTO(client);
    }

    public ClientDTO clientSave(Client client) {
        client = userRepository.save(client);
        return clientMapper.clientToClientDTO(client);
    }

    public void clientDelete(UUID id) {
        userRepository.delete(getById(id));
    }

    public ClientDTO clientPut(UUID id, ClientDTO clientDTO) {
        Client client = clientMapper.clientDTOToClient(clientDTO);
        client.setId(id);
        return this.clientSave(client);
    }

    public ClientDTO clientPatch(UUID id, ClientDTO clientDTO) {
        Client client = getById(id);
        if (StringUtils.isNotBlank(clientDTO.getName())) {
            client.setName(clientDTO.getName());
        }
        if (StringUtils.isNotBlank(clientDTO.getCpf())) {
            client.setCpf(clientDTO.getCpf());
        }
        if (clientDTO.getBirthDate() != null) {
            client.setBirthDate(client.getBirthDate());
        }
        return this.clientSave(client);
    }
}
