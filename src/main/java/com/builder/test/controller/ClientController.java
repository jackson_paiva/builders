package com.builder.test.controller;

import com.builder.test.domain.Client;
import com.builder.test.dto.ClientDTO;
import com.builder.test.service.ClientService;
import com.builder.test.specification.CustomClientSpecification;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Controller
public class ClientController {

    private final ClientService clientService;

    @DeleteMapping(value = "/client/{id}")
    public ResponseEntity<String> removeClient(@Valid @PathVariable UUID id) {
        clientService.clientDelete(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("");
    }

    @GetMapping("/client/{id}")
    public ResponseEntity<Client> findByIdClient(@PathVariable UUID id) {
        Client client = clientService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(client);
    }

    @PostMapping(value = "/client", consumes = "application/json", produces = "application/json")
    public ResponseEntity<ClientDTO> createClient(@Valid @RequestBody ClientDTO clientDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(clientService.clientSave(clientDTO));
    }

    @PutMapping(value = "/client/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<ClientDTO> clientPut(@Valid @PathVariable UUID id, @Valid @RequestBody ClientDTO clientDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(clientService.clientPut(id, clientDTO));
    }

    @PatchMapping(value = "/client/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<ClientDTO> clientPatch(@Valid @PathVariable UUID id, @Valid @RequestBody ClientDTO client) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(clientService.clientPatch(id, client));
    }

    @GetMapping("/client")
    public ResponseEntity<List<ClientDTO>> findAllClientPaginator(
            Pageable pageable,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "cpf", required = false) String cpf) {

        BooleanBuilder booleanBuilder = new BooleanBuilder();
        if (StringUtils.isNotBlank(cpf)) {
            booleanBuilder.and(CustomClientSpecification.cpf(cpf));
        }
        if (StringUtils.isNotBlank(name)) {
            booleanBuilder.and(CustomClientSpecification.name(name));
        }
        List<ClientDTO> retrieve = clientService.findAllPaginator(pageable, booleanBuilder);
        return ResponseEntity.status(HttpStatus.OK).body(retrieve);
    }

}
