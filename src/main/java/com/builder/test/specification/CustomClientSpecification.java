package com.builder.test.specification;

import com.builder.test.domain.QClient;
import com.querydsl.core.types.Predicate;

public class CustomClientSpecification {

    public static Predicate cpf(String cpf) {
        return QClient.client.cpf.eq(cpf);
    }
    public static Predicate name(String name) {
        return QClient.client.name.eq(name);
    }
}
