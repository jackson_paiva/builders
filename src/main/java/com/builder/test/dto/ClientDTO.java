package com.builder.test.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
public class ClientDTO {

    private UUID id;
    private String name;
    private String cpf;

    @JsonFormat(pattern="dd/MM/yyyy")
    private LocalDate birthDate;
    private String age;

}
