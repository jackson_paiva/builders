package com.builder.test.handler;

import com.builder.test.dto.ErrorResponseDTO;
import com.builder.test.exception.NotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class HandlerException extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        List<String> details = Arrays.asList(ex.getLocalizedMessage());
        ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO("Error server", details);
        return new ResponseEntity(errorResponseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        List<String> details = new ArrayList<>();
        for (ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(error.getDefaultMessage());
        }
        ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO("Validation error", details);
        return new ResponseEntity(errorResponseDTO, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<Object> handleNotFoundException(Exception ex) {
        List<String> details = Arrays.asList(ex.getLocalizedMessage());
        ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO(ex.getMessage(), details);
        return new ResponseEntity(errorResponseDTO, HttpStatus.NOT_FOUND);
    }

}
